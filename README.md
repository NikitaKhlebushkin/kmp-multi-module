**To reproduce**

- Checkout project
- Run `./gradlew :b:build`
- Create Android project
- Place file `b-jvm-0.0.1.jar` under `android-project/app/libs/`
- Try call `B().foo()` and `B().bar()` from Android code
